const int led = 13;
const int trigPin = 9;
const int echoPin = 8;
const int levelThreshhold = 5;

long duration;
long distance;

void setup() {
  pinMode(trigPin, OUTPUT);                
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);   
  Serial.begin(9600);  
}
 

void loop() {  
              
   

  //This readies the trigPin for usage
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  //This sends out the ultra sonic signal
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  //Reads back back sound wave duration in microseconds
  duration = pulseIn(echoPin, HIGH);

  //Fancy mathematics that magically calculates distance 
  distance= duration*0.034/2;
  Serial.println(distance);

  /*if(distance <= levelThreshhold){
    Serial.println("Bin is full please empty!");
  }
  else{
     Serial.print("Distance: ");
     Serial.print(distance);
     Serial.println("cm!"); 
     }*/
    
  


  delay(1000);
}
