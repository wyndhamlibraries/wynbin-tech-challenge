import pygal, os
from pygal.style import DarkColorizedStyle

class Smartbin:

    def __init__(self, coordinates, lastestFullness, reason, serialnumber, description, position, agethreshhold, fullnessThreshHold, timestamp):
        self.coordinates = coordinates
        self.lastestFullness = lastestFullness
        self.reason = reason
        self.serialnumber = serialnumber
        self.description = description
        self.position = position
        self.agethreshhold = agethreshhold
        self.fullnessThreshHold = fullnessThreshHold
        self.timestamp = timestamp
        self.percentage = 0
        if not self.lastestFullness == 0:
            self.percentage = int(self.lastestFullness / self.fullnessThreshHold *100)


    def get_bin_list(self):
        # pie_chart = pygal.Pie(style=DarkColorizedStyle, title=self.description, half_pie=True, inner_radius=.4)
        # pie_chart.show_legend = False
        # pie_chart.fill = False
        # pie_chart.background='transparent'
        # pie_chart.add('Empty', (int(self.fullnessThreshHold) - int(self.lastestFullness)))
        # pie_chart.add('Trash', int(self.lastestFullness))

        # output = pie_chart.render_data_uri()

        bin_list = [self.description, self.timestamp, self.percentage]  
        return bin_list 

    def get_bin(self):
        return "Coordinates: {}/nlastestFullness: {}/nreason: {}/nserialnumber: {}/ndescription: {}/nposition: {}/nagethreshhold: {}/nfullnessThreshHold: {}/ntimestamp: {}".format(self.coordinates, self.lastestFullness, self.reason, self.serialnumber, self.description, self.position, self.agethreshhold, self.fullnessThreshHold, self.timestamp)





    