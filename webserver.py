from flask import Flask, render_template, url_for
import requests, json, time, serial, pygal
from smartbin import Smartbin

app = Flask(__name__, static_url_path='/static')

@app.route('/')
def json_data():
    #url = 'https://data.gov.au/data/dataset/08531201-ac9f-4f5f-bb7e-ac16b1da28b4/resource/15732b49-3e50-40ce-8dfd-0efed18661f4/download/sb_fill_lvel.json'
    #return_data = requests.get(url)
    #return_data.content.json()
    smartbins = []
  
    with open('wyndham_smartbin_filllevel.json') as json_data:
        data = json.load(json_data)
        for f in data['features']:
            coordinates = f['geometry']['coordinates']
            lastestFullness = f['properties']['fill_lvl']
            reason  = f['properties']['status']
            serialnumber = f['properties']['serial_num']
            description = f['properties']['bin_detail']
            position = f['properties']['position']
            agethreshhold = f['properties']['age_thres']
            fullnessThreshHold = f['properties']['fill_thres']
            timestamp = f['properties']['timestamp']

            smartbins.append(Smartbin(coordinates, lastestFullness, reason, serialnumber, description, position, agethreshhold, fullnessThreshHold, timestamp))
            
    display = []
    row = []
    col = 0

    for x, data in enumerate(smartbins):
        if col == 5:
            display.append(row)
            row =[]
            col = 0
        else:
            row.append(data.get_bin_list())
            col += 1
    


    return render_template('index.html', list = display, timestamp = smartbins[0].timestamp, active = 0 )

@app.route('/SmartBin')
def Smart_Bin():
    try:
        ser = serial.Serial("/dev/ttyACM0", 9600) #left side usb port 2.0
        time.sleep(2)
        b = ser.readline()
        str_b = b.decode()
        distance = str_b.rstrip()
        ser.close()
    except: 
            distance = 1500 

    FullThreshhold = 2000
    pie_chart = pygal.Pie(title= "Smart Bin Fullness", half_pie=True, inner_radius=.4)
    pie_chart.show_legend = False
    #pie_chart.background='transparent'
    pie_chart.add('Empty', distance)
    pie_chart.add('Trash', int(FullThreshhold - int(distance)))
    output = pie_chart.render_data_uri()

    return render_template('smart_bin.html', distance = distance, chart = output)

@app.route('/Currentfill')
def Current_fill_level():
    url = 'https://data.gov.au/data/dataset/08531201-ac9f-4f5f-bb7e-ac16b1da28b4/resource/15732b49-3e50-40ce-8dfd-0efed18661f4/download/sb_fill_lvel.json'
    return_data = requests.get(url).json()
    smartbins = []
  
    for f in return_data['features']:
        coordinates = f['geometry']['coordinates']
        lastestFullness = f['properties']['fill_lvl']
        reason  = f['properties']['status']
        serialnumber = f['properties']['serial_num']
        description = f['properties']['bin_detail']
        position = f['properties']['position']
        agethreshhold = f['properties']['age_thres']
        fullnessThreshHold = f['properties']['fill_thres']
        timestamp = f['properties']['timestamp']

        smartbins.append(Smartbin(coordinates, lastestFullness, reason, serialnumber, description, position, agethreshhold, fullnessThreshHold, timestamp))
            
    display = []
    row = []
    col = 0

    for x, data in enumerate(smartbins):
        if col == 5:
            display.append(row)
            row =[]
            col = 0
        else:
            row.append(data.get_bin_list())
            col += 1
    


    return render_template('index.html', list = display, timestamp = smartbins[0].timestamp, active = 1 )
 



if __name__ == '__main__':
    app.debug = True
    app.run()

